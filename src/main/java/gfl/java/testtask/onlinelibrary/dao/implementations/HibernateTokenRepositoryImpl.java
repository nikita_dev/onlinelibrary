package gfl.java.testtask.onlinelibrary.dao.implementations;

import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import gfl.java.testtask.onlinelibrary.model.PersistentLogin;

@Transactional
@Repository("tokenRepositoryDao")
public class HibernateTokenRepositoryImpl extends DaoImpl<String, PersistentLogin> implements PersistentTokenRepository {

	@Override
	public void createNewToken(PersistentRememberMeToken token) {
		PersistentLogin persistentLogin = new PersistentLogin();
        
		persistentLogin
            .setUsername(token.getUsername())
            .setSeries(token.getSeries())
            .setToken(token.getTokenValue())
            .setLastUsed(token.getDate());
        
		this.save(persistentLogin);
	}

	@Override
	public PersistentRememberMeToken getTokenForSeries(String seriesId) {
		try {
			Criteria crit = this.getEntityCriteria();
            
			crit.add(Restrictions.eq("series", seriesId));
            
			PersistentLogin persistentLogin = (PersistentLogin) crit.uniqueResult();

			return new PersistentRememberMeToken(
                persistentLogin.getUsername(), 
                persistentLogin.getSeries(),
				persistentLogin.getToken(), 
                persistentLogin.getLastUsed()
            );
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void removeUserTokens(String username) {
		Criteria crit = this.getEntityCriteria();
        
		crit.add(Restrictions.eq("userName", username));
        
		PersistentLogin persistentLogin = (PersistentLogin) crit.uniqueResult();
        
		if (persistentLogin != null) {
			this.delete(persistentLogin);
		}
	}

	@Override
	public void updateToken(String seriesId, String tokenValue, Date lastUsed) {
		PersistentLogin persistentLogin = this.findById(seriesId);
        
		persistentLogin
            .setToken(tokenValue)
            .setLastUsed(lastUsed);
        
		this.update(persistentLogin);
	}
}