package gfl.java.testtask.onlinelibrary.dao.implementations;

import gfl.java.testtask.onlinelibrary.model.Book;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import gfl.java.testtask.onlinelibrary.dao.BookDaoInterface;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

@Transactional
@Repository("bookDao")
public class BookDaoImpl extends DaoImpl<Integer, Book> implements BookDaoInterface {
	
	@Override
	public List<Book> findByNameOrDescr(String searchTarget, String orderBy) {
		Criterion c1 = Restrictions.ilike("title", searchTarget, MatchMode.ANYWHERE);
		Criterion c2 = Restrictions.ilike("description", searchTarget, MatchMode.ANYWHERE);
		
		Criteria criteria = this.getEntityCriteria().add(Restrictions.or(c1, c2)).addOrder(Order.desc(orderBy));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return criteria.list();
	}
	
	@Override
	public Integer getEntityCount() {
		return 
			this.getEntityCriteria()
			.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
			.list()
			.size();
	}
}