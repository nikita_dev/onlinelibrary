package gfl.java.testtask.onlinelibrary.dao;

import gfl.java.testtask.onlinelibrary.model.User;

public interface UserDaoInterface extends DaoInterface<Integer, User> {
	User findByLogin(String login);
}