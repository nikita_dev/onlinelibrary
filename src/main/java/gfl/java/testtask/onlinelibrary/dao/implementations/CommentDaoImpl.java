package gfl.java.testtask.onlinelibrary.dao.implementations;

import gfl.java.testtask.onlinelibrary.model.Comment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import gfl.java.testtask.onlinelibrary.dao.CommentDaoInterface;

@Transactional
@Repository("commentDao")
public class CommentDaoImpl extends DaoImpl<Integer, Comment> implements CommentDaoInterface {
    
}