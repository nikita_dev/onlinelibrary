package gfl.java.testtask.onlinelibrary.dao.implementations;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import gfl.java.testtask.onlinelibrary.model.User;
import gfl.java.testtask.onlinelibrary.dao.UserDaoInterface;

@Repository("userDao")
public class UserDaoImpl extends DaoImpl<Integer, User> implements UserDaoInterface {

	@Override
	public User findByLogin(String login) {
		Criteria crit = this.getEntityCriteria();
        
		crit.add(Restrictions.eq("login", login));
        
		return (User) crit.uniqueResult();
	}
}