package gfl.java.testtask.onlinelibrary.dao.implementations;

import gfl.java.testtask.onlinelibrary.dao.VoteDaoInterface;
import gfl.java.testtask.onlinelibrary.model.Book;
import gfl.java.testtask.onlinelibrary.model.User;
import gfl.java.testtask.onlinelibrary.model.Vote;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("voteDao")
public class VoteDaoImpl extends DaoImpl<Integer, Vote> implements VoteDaoInterface {

    @Override
    public Vote findByBookAndUser(Book book, User user) {
        Criterion c1 = Restrictions.eq("book", book);
		Criterion c2 = Restrictions.eq("user", user);
        
        Criteria criteria = this.getEntityCriteria().add(Restrictions.and(c1, c2));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        
		return (Vote) criteria.uniqueResult();
    }
}