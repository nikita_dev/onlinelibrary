package gfl.java.testtask.onlinelibrary.dao;

import gfl.java.testtask.onlinelibrary.model.Comment;

public interface CommentDaoInterface extends DaoInterface<Integer, Comment> {
    
}