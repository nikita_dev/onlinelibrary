package gfl.java.testtask.onlinelibrary.dao;

import java.io.Serializable;
import java.util.List;

public interface DaoInterface<PK extends Serializable, T> {
	void save(T entity);

    void delete(T entity);

    void update(T entity);

    T findById(PK id);

    List<T> findAll(String orderBy);
}