package gfl.java.testtask.onlinelibrary.dao.implementations;

import gfl.java.testtask.onlinelibrary.dao.DaoInterface;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public abstract class DaoImpl<PK extends Serializable, T> implements DaoInterface<PK, T> {
	
	protected final Class<T> persistentClass;
	
	@Autowired
	protected SessionFactory sessionFactory;
	
	protected Criteria getEntityCriteria() {
		return this.getCurrentSession().createCriteria(this.persistentClass);
	}
	
	protected Session getCurrentSession() {
		return this.sessionFactory.getCurrentSession();
	}
	
	public DaoImpl() {
		this.persistentClass = (Class<T>) (
			(ParameterizedType) this.getClass().getGenericSuperclass()
		).getActualTypeArguments()[1];
	}

	@Override
	public T findById(PK id) {
		return (T) this.getCurrentSession().get(this.persistentClass, id);
	}

	@Override
	public List<T> findAll(String orderBy) {
		Criteria criteria = this.getEntityCriteria().addOrder(Order.desc(orderBy));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return criteria.list();
	}
	
	@Override
	public void save(T entity) {
		this.getCurrentSession().persist(entity);
	}
	
	@Override
    public void update(T entity) {
		this.getCurrentSession().update(entity);
    }
	
	@Override
	public void delete(T entity) {
		this.getCurrentSession().delete(entity);
	}
}