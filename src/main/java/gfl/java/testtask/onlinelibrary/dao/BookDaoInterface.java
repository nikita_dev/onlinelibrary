package gfl.java.testtask.onlinelibrary.dao;

import gfl.java.testtask.onlinelibrary.model.Book;
import java.util.List;

public interface BookDaoInterface extends DaoInterface<Integer, Book> {		
    List<Book> findByNameOrDescr(String searchTarget, String orderBy);
	Integer getEntityCount();
}