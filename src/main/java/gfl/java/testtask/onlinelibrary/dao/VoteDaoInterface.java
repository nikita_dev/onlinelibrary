package gfl.java.testtask.onlinelibrary.dao;

import gfl.java.testtask.onlinelibrary.model.Book;
import gfl.java.testtask.onlinelibrary.model.User;
import gfl.java.testtask.onlinelibrary.model.Vote;

public interface VoteDaoInterface extends DaoInterface<Integer, Vote> {
    Vote findByBookAndUser(Book book, User user);
}
