package gfl.java.testtask.onlinelibrary.model;

public enum BookThumbnailExtension {
    GIF, JPG, PNG;
}