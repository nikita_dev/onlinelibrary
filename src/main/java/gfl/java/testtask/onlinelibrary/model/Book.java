package gfl.java.testtask.onlinelibrary.model;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Past;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "books")
public class Book implements Serializable {
    private static final String PUBLISH_DATE_FORMAT = "dd-MM-yyyy";
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "book_id", nullable = false)
	private Integer id;
	
	@NotEmpty
	@Column(nullable = false)
	private String title;
	
	@NotEmpty
	@Column(nullable = false, length = 5000)
	private String description;
	
	@Column(name = "thumbnail_path", nullable = true)
	private String thumbnailPath;
	
	@Column(name = "file_path", nullable = false)
	private String filePath;
	
	@NotEmpty
	@Column(nullable = false)
	private String publisher;
	
	@NotEmpty
	@Column(nullable = false)
	private String authors;
	
	@Past
	@DateTimeFormat(pattern = PUBLISH_DATE_FORMAT)
    @Column(name = "publish_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date publishDate;
	
    @OrderBy("date")
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "book", cascade = CascadeType.ALL)
	private Set<Comment> comments;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "book", cascade = CascadeType.ALL)
    private Set<Vote> votes;
    
    public Book() {
        this.comments = new LinkedHashSet<>();
        this.votes = new LinkedHashSet<>();
    }
	
	public Integer getId() {
		return this.id;
	}
	
	public Book setId(Integer id) {
		this.id = id;
		
		return this;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public Book setTitle(String title) {
		this.title = title;
		
		return this;
	}

	public String getDescription() {
		return this.description;
	}

	public Book setDescription(String description) {
		this.description = description;
		
		return this;
	}
	
	public String getThumbnailPath() {
		return thumbnailPath;
	}

	public Book setThumbnailPath(String thumbnailPath) {
		this.thumbnailPath = thumbnailPath;
		
		return this;
	}
	
	public String getFilePath() {
		return filePath;
	}
    
	public Book setFilePath(String filePath) {
		this.filePath = filePath;
        
        return this;
	}

	public String getPublisher() {
		return this.publisher;
	}

	public Book setPublisher(String publisher) {
		this.publisher = publisher;
		
		return this;
	}

	public String getAuthors() {
		return this.authors;
	}

	public Book setAuthors(String authors) {
		this.authors = authors;
		
		return this;
	}

	public Date getPublishDate() {
		return this.publishDate;
	}

	public Book setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
		
		return this;
	}
	
	public Set<Comment> getComments() {
		return this.comments;
	}
	
	public Book setComments(Set<Comment> comments) {
		this.comments = comments;
		
		return this;
	}
    
    public Set<Vote> getVotes() {
        return votes;
    }

    public Book setVotes(Set<Vote> votes) {
        this.votes = votes;
        
        return this;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
        int result = 1;
        result = prime * result + (this.title.hashCode() ^ (this.description.hashCode() >>> 31 ));
        return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Book other = (Book) obj;
		if (!Objects.equals(this.title, other.title)) {
			return false;
		}
		if (!Objects.equals(this.description, other.description)) {
			return false;
		}
		if (!Objects.equals(this.publishDate, other.publishDate)) {
			return false;
		}
		return true;
	}
	
	public String getThumbnailBase64() {
		
		String extension = FilenameUtils.getExtension(this.thumbnailPath);
		
		try {
			return String.format(
				"data:%s;base64,%s", 
				extension, 
				Base64.getEncoder().encodeToString(
					FileUtils.readFileToByteArray(
						new File(this.thumbnailPath)
					)
				)
			);
		} catch (IOException | NullPointerException ex) {
			return "";
		}
	}
    
    public int getRating() {
        return this.votes.stream().mapToInt(vote -> vote.getVote()).sum();
    }
    
    public boolean hasUserVoted(User user) {
        return this.votes.stream().anyMatch((vote) -> (vote.getUser().equals(user)));
    }
	
	@Override
	public String toString() {
        String publishDate = new SimpleDateFormat(PUBLISH_DATE_FORMAT).format(this.publishDate);
        
		return String.format("%d%s%s", this.id, this.title, publishDate);
	}
}