package gfl.java.testtask.onlinelibrary.model;

public enum BookFileExtention {
    DOC, ODT, PDF, RTF, TXT;
}
