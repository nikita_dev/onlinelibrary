package gfl.java.testtask.onlinelibrary.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "books_votes")
public class Vote implements Serializable {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vote_id", nullable = false)
	private Integer id;
    
    @ManyToOne
	@JoinColumn(name = "book_id", nullable = false)
    private Book book;
    
    @ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
    private User user;
    
    @Column(nullable = false)
    private Byte vote;
    
    @Column(name = "vote_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
    
    public Vote() {
        this.date = new Date();
    }

    public Integer getId() {
        return id;
    }

    public Vote setId(Integer id) {
        this.id = id;
        
        return this;
    }

    public Book getBook() {
        return book;
    }

    public Vote setBook(Book book) {
        this.book = book;
        
        return this;
    }

    public User getUser() {
        return user;
    }

    public Vote setUser(User user) {
        this.user = user;
        
        return this;
    }

    public Byte getVote() {
        return vote;
    }

    public Vote setVote(Byte vote) {
        this.vote = vote;
        
        return this;
    }
    
    public Vote setVote(Boolean vote) {
        
        Byte result = vote ? (byte) 1 : (byte) -1;
        
        this.vote = result;
        
        return this;
    }
    
    public Date getDate() {
        return date;
    }

    public Vote setDate(Date date) {
        this.date = date;
        
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.book);
        hash = 59 * hash + Objects.hashCode(this.user);
        hash = 59 * hash + Objects.hashCode(this.vote);
        hash = 59 * hash + Objects.hashCode(this.date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vote other = (Vote) obj;
        if (!Objects.equals(this.book, other.book)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.vote, other.vote)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }
}