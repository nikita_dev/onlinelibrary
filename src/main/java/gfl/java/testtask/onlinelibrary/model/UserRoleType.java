package gfl.java.testtask.onlinelibrary.model;

public enum UserRoleType {
    ROLE_USER, ROLE_ADMIN
}