package gfl.java.testtask.onlinelibrary.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="persistent_logins")
public class PersistentLogin implements Serializable {

	@Id
	private String series;

	@Column(name="user_name", unique = true, nullable = false)
	private String userName;
	
	@Column(unique = true, nullable = false)
	private String token;
	
    @Column(name = "last_used", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUsed;

	public String getSeries() {
		return series;
	}

	public PersistentLogin setSeries(String series) {
		this.series = series;
        
        return this;
	}

	public String getUsername() {
		return this.userName;
	}

	public PersistentLogin setUsername(String userName) {
		this.userName = userName;
        
        return this;
	}

	public String getToken() {
		return token;
	}

	public PersistentLogin setToken(String token) {
		this.token = token;
        
        return this;
	}

	public Date getLastUsed() {
		return this.lastUsed;
	}

	public PersistentLogin setLastUsed(Date lastUsed) {
		this.lastUsed = lastUsed;
        
        return this;
	}
}