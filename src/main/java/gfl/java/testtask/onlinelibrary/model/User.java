package gfl.java.testtask.onlinelibrary.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Email;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "users")
public class User implements Serializable {

    private static final String LOGIN_PASSW_REGEXP = "[a-z0-9_]{4,64}";
    
	@Id
    @Column(name = "user_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

    @Pattern(regexp = LOGIN_PASSW_REGEXP, flags = Pattern.Flag.CASE_INSENSITIVE)
	@Column(unique = true, nullable = false)
	private String login;
	
	@Column(nullable = false, updatable = false)
	private String password;
	
	@NotNull
	@Column(name = "first_name", nullable = false)
	private String firstName;

	@NotNull
	@Column(name = "last_name", nullable = false)
	private String lastName;

	@NotEmpty
    @Email
	@Column(nullable = false)
	private String email;

    @Column(name = "user_role", nullable = false)
	private String userRole;
    
    public User() {
        this.userRole = UserRoleType.ROLE_USER.name();
    }

	public Integer getId() {
		return this.id;
	}

	public User setId(Integer id) {
		this.id = id;
        
        return this;
	}

	public String getLogin() {
		return this.login;
	}

	public User setLogin(String login) {
		this.login = login;
        
        return this;
	}

	public String getPassword() {
		return this.password;
	}

	public User setPassword(String password) {
		this.password = password;
        
        return this;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public User setFirstName(String firstName) {
		this.firstName = firstName;
        
        return this;
	}

	public String getLastName() {
		return this.lastName;
	}

	public User setLastName(String lastName) {
		this.lastName = lastName;
        
        return this;
	}

	public String getEmail() {
		return this.email;
	}

	public User setEmail(String email) {
		this.email = email;
        
        return this;
	}

	public String getUserRole() {
		return this.userRole;
	}

	public User setUserRole(String userRole) {
		this.userRole = userRole;
        
        return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		result = prime * result + ((this.login == null) ? 0 : this.login.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		if (this.id == null) {
			if (other.id != null)
				return false;
		} else if (!this.id.equals(other.id))
			return false;
		if (this.login == null) {
			if (other.login != null)
				return false;
		} else if (!this.login.equals(other.login))
			return false;
		return true;
	}
    
	@Override
	public String toString() {
		return "User [id=" + this.id + ", login=" + this.login + ", password=" + this.password
            + ", firstName=" + this.firstName + ", lastName=" + this.lastName
            + ", email=" + this.email + "]";
	}	
}