package gfl.java.testtask.onlinelibrary.model;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "comments")
public class Comment implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "comment_id", nullable = false)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "parent_id", nullable = true)
	private Comment parentComment;
	
    @OrderBy("date")
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "parentComment", cascade = CascadeType.ALL)
	private Set<Comment> subComments;
	
	@ManyToOne
	@JoinColumn(name = "book_id", nullable = false)
	private Book book;
	
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;
	
    @NotEmpty
	@Column(name = "content", nullable = false)
	private String text;
	
	@Column(name = "publish_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	public Comment() {
		this.parentComment = null;
		this.subComments = new LinkedHashSet<>();
		this.date = new Date();
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public Comment setId(Integer id) {
		this.id = id;
		
		return this;
	}
	
	public Comment getParentComment() {
		return parentComment;
	}

	public Comment setParentComment(Comment parentComment) {
		this.parentComment = parentComment;
		
		return this;
	}
	
	public Set<Comment> getSubComments() {
		return subComments;
	}

	public Comment setSubComments(Set<Comment> subComments) {
		this.subComments = subComments;
		
		return this;
	}
	
	public Book getBook() {
		return this.book;
	}
	
	public Comment setBook(Book book) {
		this.book = book;
		
		return this;
	}
	
	public User getUser() {
		return this.user;
	}
	
	public Comment setUser(User user) {
		this.user = user;
		
		return this;
	}
	
	public String getText() {
		return this.text;
	}
	
	public Comment setText(String text) {
		this.text = text;
		
		return this;
	}
	
	public Date getDate() {
		return this.date;
	}
	
	public Comment setDate(Date date) {
		this.date = date;
		
		return this;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
        int result = 1;
        result = prime * result + (int) (this.date.hashCode()) ^ (this.user.getId() ^ (this.book.getId() >>> 32));
        return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Comment other = (Comment) obj;
		if (!Objects.equals(this.text, other.text)) {
			return false;
		}
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		if (!Objects.equals(this.book, other.book)) {
			return false;
		}
		if (!Objects.equals(this.date, other.date)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "Comment (id=" + this.id + ", title=" + this.text + ")";
	}
}