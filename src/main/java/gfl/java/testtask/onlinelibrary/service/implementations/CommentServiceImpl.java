package gfl.java.testtask.onlinelibrary.service.implementations;

import gfl.java.testtask.onlinelibrary.dao.CommentDaoInterface;
import gfl.java.testtask.onlinelibrary.dao.DaoInterface;
import gfl.java.testtask.onlinelibrary.model.Comment;
import gfl.java.testtask.onlinelibrary.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("commentService")
public class CommentServiceImpl extends ServiceImpl<Comment> implements CommentService {
    
    @Autowired
    protected CommentDaoInterface dao;

    @Override
    protected DaoInterface<Integer, Comment> daoFactory() {
        return this.dao;
    }
}