package gfl.java.testtask.onlinelibrary.service.implementations;

import gfl.java.testtask.onlinelibrary.dao.DaoInterface;
import gfl.java.testtask.onlinelibrary.dao.VoteDaoInterface;
import gfl.java.testtask.onlinelibrary.model.Book;
import gfl.java.testtask.onlinelibrary.model.User;
import gfl.java.testtask.onlinelibrary.model.Vote;
import gfl.java.testtask.onlinelibrary.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("voteService")
public class VoteServiceImpl extends ServiceImpl<Vote> implements VoteService {

    @Autowired
    protected VoteDaoInterface dao;
    
    @Override
    protected DaoInterface<Integer, Vote> daoFactory() {
        return this.dao;
    }

    @Override
    public Vote findByBookAndUser(Book book, User user) {
        return this.dao.findByBookAndUser(book, user);
    }
}