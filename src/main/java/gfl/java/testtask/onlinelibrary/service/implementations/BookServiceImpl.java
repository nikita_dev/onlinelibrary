package gfl.java.testtask.onlinelibrary.service.implementations;

import gfl.java.testtask.onlinelibrary.dao.BookDaoInterface;
import gfl.java.testtask.onlinelibrary.dao.DaoInterface;
import gfl.java.testtask.onlinelibrary.model.Book;
import gfl.java.testtask.onlinelibrary.service.BookService;
import java.io.File;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("bookService")
public class BookServiceImpl extends ServiceImpl<Book> implements BookService {
    
    @Autowired
    protected BookDaoInterface dao;

    @Override
    protected DaoInterface<Integer, Book> daoFactory() {
        return this.dao;
    }
	
	@Override
	public List<Book> findAll(String orderBy, Integer pageNumber) {
        List<Book> books = this.dao.findAll(orderBy);
        
        int lastItemOnPage = ITEMS_PER_PAGE * pageNumber;
        
        if (lastItemOnPage > books.size()) {
            lastItemOnPage = books.size();
        }
        
		return books.subList(ITEMS_PER_PAGE * (pageNumber - 1), lastItemOnPage);
	}
    
    @Override
    public void delete(Book book) {
        try {
            File thumbnail = new File(book.getThumbnailPath());
        
            if (thumbnail.exists()) {
                thumbnail.delete();
            }
            
            File bookFile = new File(book.getFilePath());
            
            if (bookFile.exists()) {
                bookFile.delete();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        
        super.delete(book);
    }

	@Override
	public List<Book> findByNameOrDescr(String searchTarget, String orderBy) {
		return this.dao.findByNameOrDescr(searchTarget, orderBy);
	}

	@Override
	public Integer getPageCount() {
		return (int) Math.ceil(this.dao.getEntityCount() / (double)ITEMS_PER_PAGE);
	}
}