package gfl.java.testtask.onlinelibrary.service.implementations;

import gfl.java.testtask.onlinelibrary.dao.DaoInterface;
import gfl.java.testtask.onlinelibrary.service.ServiceInterface;
import java.util.List;

public abstract class ServiceImpl<T> implements ServiceInterface<T> {
    
    protected abstract DaoInterface<Integer, T> daoFactory();
    
    @Override
    public T findById(Integer id) {
        return this.daoFactory().findById(id);
    }

    @Override
    public List<T> findAll(String orderBy) {
        return this.daoFactory().findAll(orderBy);
    }

    @Override
    public void save(T entity) {
        this.daoFactory().save(entity);
    }

    @Override
    public void update(T entity) {
        this.daoFactory().update(entity);
    }

    @Override
    public void delete(T entity) {
        this.daoFactory().delete(entity);
    }
}