package gfl.java.testtask.onlinelibrary.service.implementations;

import gfl.java.testtask.onlinelibrary.dao.DaoInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gfl.java.testtask.onlinelibrary.model.User;
import gfl.java.testtask.onlinelibrary.service.UserService;
import gfl.java.testtask.onlinelibrary.dao.UserDaoInterface;

@Service("userService")
@Transactional
public class UserServiceImpl extends ServiceImpl<User> implements UserService {

    @Autowired
    protected PasswordEncoder passwordEncoder;
    
	@Autowired
	protected UserDaoInterface dao;
    
    @Override
    protected DaoInterface<Integer, User> daoFactory() {
        return this.dao;
    }

	@Override
	public User findByLogin(String login) {
		return this.dao.findByLogin(login);
	}

	@Override
	public void save(User user) {
		user.setPassword(
            this.passwordEncoder.encode(user.getPassword())
        );
        
		super.save(user);
	}

	@Override
	public boolean isUserLoginUnique(Integer id, String login) {
        User user = this.findByLogin(login);
        
		return ( user == null || ((id != null) && (user.getId() == id)));
	}
}