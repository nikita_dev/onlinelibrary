package gfl.java.testtask.onlinelibrary.service;

import gfl.java.testtask.onlinelibrary.model.User;

public interface UserService extends ServiceInterface<User> {
	User findByLogin(String login);
	
	boolean isUserLoginUnique(Integer id, String login);
}