package gfl.java.testtask.onlinelibrary.service;

import gfl.java.testtask.onlinelibrary.model.Comment;

public interface CommentService extends ServiceInterface<Comment> {
    
}