package gfl.java.testtask.onlinelibrary.service;

import java.util.List;

public interface ServiceInterface<T> {
    public T findById(Integer id);
    
    public List<T> findAll(String orderBy);
    
    public void save(T entity);
    
    public void update(T entity);
    
    public void delete(T entity);
}