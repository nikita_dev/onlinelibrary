package gfl.java.testtask.onlinelibrary.service;

import gfl.java.testtask.onlinelibrary.model.Book;
import gfl.java.testtask.onlinelibrary.model.User;
import gfl.java.testtask.onlinelibrary.model.Vote;

public interface VoteService extends ServiceInterface<Vote> {
    Vote findByBookAndUser(Book book, User user);
}