package gfl.java.testtask.onlinelibrary.service;

import gfl.java.testtask.onlinelibrary.model.Book;
import java.util.List;

public interface BookService extends ServiceInterface<Book> {
	int ITEMS_PER_PAGE = 1;
	
	List<Book> findAll(String orderBy, Integer pageNumber);
    List<Book> findByNameOrDescr(String searchTarget, String orderBy);
	Integer getPageCount();
}