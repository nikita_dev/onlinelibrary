package gfl.java.testtask.onlinelibrary.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gfl.java.testtask.onlinelibrary.model.User;
import gfl.java.testtask.onlinelibrary.service.UserService;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService{
    
	@Autowired
	private UserService userService;
    
    private List<GrantedAuthority> getGrantedAuthorities(User user){
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        
        authorities.add(new SimpleGrantedAuthority(user.getUserRole()));
		
		return authorities;
	}
	
	@Transactional(readOnly=true)
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		User user = userService.findByLogin(login);
        
		if(user==null){
			throw new UsernameNotFoundException("Username not found");
		}
        
		return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), 
            true, true, true, true, getGrantedAuthorities(user));
	}
}