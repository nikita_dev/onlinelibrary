package gfl.java.testtask.onlinelibrary.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    //seconds
    private static final int TOKEN_VALIDITY_DURATION = 60 * 60 * 24;
    
	@Autowired
	@Qualifier("customUserDetailsService")
	private UserDetailsService userDetailsService;

	@Autowired
	private PersistentTokenRepository tokenRepository;
    
    @Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
			.authorizeRequests().antMatchers("/", "/book/*", "/add_comment/*", "/book/download/*")
				.access("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
			.and().authorizeRequests().antMatchers("/admin/list_user", "/admin/edit_user/*", "/admin/delete_user/*", "/admin/add_book", "/admin/edit_book/*", "/admin/delete_book/*")
				.access("hasRole('ROLE_ADMIN')")
            .and().formLogin().loginPage("/user/login").loginProcessingUrl("/user/login")
                .usernameParameter("login").passwordParameter("password")
            .and().rememberMe().rememberMeParameter("remember-me")
                .tokenRepository(this.tokenRepository)
                .tokenValiditySeconds(TOKEN_VALIDITY_DURATION)
            .and().csrf().and().exceptionHandling().accessDeniedPage("/user/access_denied");
	}

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.userDetailsService);
		auth.authenticationProvider(this.authenticationProvider());
	}
    
    @Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        
		authenticationProvider.setUserDetailsService(this.userDetailsService);
		authenticationProvider.setPasswordEncoder(this.passwordEncoder());
        
		return authenticationProvider;
	}

	@Bean
	public PersistentTokenBasedRememberMeServices getPersistentTokenBasedRememberMeServices() {
		PersistentTokenBasedRememberMeServices tokenBasedservice = new PersistentTokenBasedRememberMeServices(
            "remember-me", 
            this.userDetailsService, 
            this.tokenRepository
        );
        
		return tokenBasedservice;
	}

	@Bean
	public AuthenticationTrustResolver getAuthenticationTrustResolver() {
		return new AuthenticationTrustResolverImpl();
	}
}