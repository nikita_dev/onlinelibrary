package gfl.java.testtask.onlinelibrary.validator;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class RegistrationForm {
	
	private final static String LOGIN_PASSWORD_PATTERN = "^[a-z0-9_]{4,64}$";
	
	private Integer id;
	
	@Pattern(regexp = LOGIN_PASSWORD_PATTERN, flags = Pattern.Flag.CASE_INSENSITIVE, 
		message = "Login must contain at least 4 symbols and not more than 64 "
				+ "symbols. Only letters, digits and underscore symbols are allowed.")
	private String login;
	
	@Pattern(regexp = LOGIN_PASSWORD_PATTERN, flags = Pattern.Flag.CASE_INSENSITIVE,
		message = "Password must contain at least 4 symbols and not more than 64 "
				+ "symbols. Only letters, digits and underscore symbols are allowed.")
	private String password;
	
	private String confirmationPassword;
	
	@NotEmpty(message = "Email cannot be blank")
	@Email(message = "Invalid email")
	private String Email;
	
	private String firstName;
	
	private String lastName;
	
	@AssertTrue(message = "Passwords are not identical")
	public boolean isIdenticalPasswords() {
		return 
			this.password == null || 
			this.password.equals(this.confirmationPassword);
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public RegistrationForm setId(Integer id) {
		this.id = id;
        
        return this;
	}

	public String getLogin() {
		return login;
	}

	public RegistrationForm setLogin(String login) {
		this.login = login;
        
        return this;
	}

	public String getPassword() {
		return password;
	}

	public RegistrationForm setPassword(String password) {
		this.password = password;
        
        return this;
	}

	public String getConfirmationPassword() {
		return confirmationPassword;
	}

	public RegistrationForm setConfirmationPassword(String confirmationPassword) {
		this.confirmationPassword = confirmationPassword;
        
        return this;
	}

	public String getEmail() {
		return Email;
	}

	public RegistrationForm setEmail(String Email) {
		this.Email = Email;
        
        return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public RegistrationForm setFirstName(String firstName) {
		this.firstName = firstName;
        
        return this;
	}

	public String getLastName() {
		return lastName;
	}

	public RegistrationForm setLastName(String lastName) {
		this.lastName = lastName;
        
        return this;
	}
}