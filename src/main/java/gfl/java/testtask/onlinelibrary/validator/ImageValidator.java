package gfl.java.testtask.onlinelibrary.validator;

import gfl.java.testtask.onlinelibrary.model.BookThumbnailExtension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;

@Component
public class ImageValidator extends BaseValidator {

	private static final int MAX_WIDTH = 800;
	
	private static final int MAX_HEIGHT = 600;
	
	private void validateExtension(MultipartFile image, Errors errors) {
        if (!this.isValidExtension(image, BookThumbnailExtension.class)) {
            errors.rejectValue("thumbnailPath", "extension.image");
        }
	}
	
	private void validateSize(MultipartFile image, Errors errors) {
		try {
			BufferedImage bi = ImageIO.read(image.getInputStream());
			
			if (bi.getHeight() > MAX_HEIGHT || bi.getWidth() > MAX_WIDTH) {
				errors.rejectValue("thumbnailPath", "size.image");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void validate(Object object, Errors errors) {
        MultipartFile image = (MultipartFile) object;
        
		if (null != image && 0 != image.getSize()) {
			this.validateExtension(image, errors);
			this.validateSize(image, errors);
		}
	}
}