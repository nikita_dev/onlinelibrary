package gfl.java.testtask.onlinelibrary.validator;

import org.apache.commons.io.FilenameUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

abstract public class BaseValidator implements Validator {
    
    @Override
	public boolean supports(Class<?> type) {
		return MultipartFile.class.isAssignableFrom(type);
	}
    
    protected <E extends Enum<E>> boolean isValidExtension(MultipartFile file, Class<E> extensions) {
        for (E extension : extensions.getEnumConstants()) {
			if (extension.name().equals(
					FilenameUtils.getExtension(
						file.getOriginalFilename().toUpperCase()
					)
				)
			) {
				return true;
			}
		}
        
        return false;
    }
}