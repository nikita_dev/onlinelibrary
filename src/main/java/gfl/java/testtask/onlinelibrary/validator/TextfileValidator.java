package gfl.java.testtask.onlinelibrary.validator;

import gfl.java.testtask.onlinelibrary.model.BookFileExtention;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;

@Component
public class TextfileValidator extends BaseValidator {
    private static final long MAX_SIZE = 20 * 1024 * 1024; //bytes
    
    private void validateExtension(MultipartFile textFile, Errors errors) {
		if (!this.isValidExtension(textFile, BookFileExtention.class)) {
            errors.rejectValue("filePath", "extension.textFile");
        }
	}
    
    private void validateSize(MultipartFile textFile, Errors errors) {
        if (textFile.getSize() > MAX_SIZE) {
            errors.rejectValue("filePath", "size.textFile");
        }
    }
    
    @Override
    public void validate(Object object, Errors errors) {
        MultipartFile textFile = (MultipartFile) object;
        
        if (null != textFile && 0 != textFile.getSize()) {
            this.validateExtension(textFile, errors);
            this.validateSize(textFile, errors);
        }
        else {
            errors.rejectValue("filePath", "missing.textFile");
        }
    }
}