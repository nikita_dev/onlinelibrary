package gfl.java.testtask.onlinelibrary.file_utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

public class FileUtils {
	private static final String DESTINATION_FOLDER_NAME = "resources";
	
	private MultipartFile sourceFile;
	
	private File destinationFile;
	
	private String destinationPath;
    
    private String oldName;
    
    private void deleteOldFile() {
        new File(this.oldName).delete();
    }
	
	public FileUtils(MultipartFile file, HttpServletRequest request, String oldName) throws FileNotFoundException {
        if (null == file || 0 == file.getSize()) {
            throw new FileNotFoundException();
        }
		this.sourceFile = file;
        
        this.oldName = oldName;
		
		String sessionPath = request.getSession().getServletContext().getRealPath("/");
		
		this.destinationPath = String.format("%s%s", sessionPath, DESTINATION_FOLDER_NAME);
		
		File destinationDirectory = new File(this.destinationPath);
			
		if (!destinationDirectory.exists()) {
			destinationDirectory.mkdir();
		}
	}
	
	public boolean couldSaveFile() {
        if (null == this.sourceFile) {
            return true;
        }
		try {
            if (null != this.oldName) {
                this.deleteOldFile();
            }
            
			this.destinationFile = File.createTempFile("file", "." + FilenameUtils.getExtension(this.sourceFile.getOriginalFilename()), new File(this.destinationPath));
			
			FileCopyUtils.copy(this.sourceFile.getBytes(), this.destinationFile);
			
			return true;
		} catch (IOException ex) {
			return false;
		}
	}
	
	public String getResultPath() {
		return null == this.destinationFile ? 
            null : 
            this.destinationFile.getAbsolutePath();
	}
}