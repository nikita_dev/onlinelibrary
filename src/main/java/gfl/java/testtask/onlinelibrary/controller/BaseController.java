package gfl.java.testtask.onlinelibrary.controller;

import gfl.java.testtask.onlinelibrary.service.BookService;
import gfl.java.testtask.onlinelibrary.service.UserService;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.FieldError;

public abstract class BaseController {
    
    @Autowired
    protected BookService bookService;
	
	@Autowired
	protected UserService userService;
    
    @Autowired
	protected MessageSource messageSource;
    
	protected String getPrincipal(){
		String userName = null;
        
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails)principal).getUsername();
		} else {
			userName = principal.toString();
		}
        
		return userName;
	}
    
    protected FieldError getFieldError(String name, String field, String message, String objectProp) {
        return new FieldError(name, field, 
            this.messageSource.getMessage(message, new String[]{ objectProp }, Locale.getDefault())
        );
    }
}