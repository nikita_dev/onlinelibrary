package gfl.java.testtask.onlinelibrary.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.validation.FieldError;

import gfl.java.testtask.onlinelibrary.model.User;
import gfl.java.testtask.onlinelibrary.validator.RegistrationForm;

@Controller
@RequestMapping("/user")
@SessionAttributes("roles")
public class UserController extends BaseController {

	@Autowired
	private PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;
	
	@Autowired
	private AuthenticationTrustResolver authenticationTrustResolver;
	
	private boolean isCurrentAuthenticationAnonymous() {
	    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
	    return this.authenticationTrustResolver.isAnonymous(authentication);
	}

	@RequestMapping(value = { "/registration" }, method = RequestMethod.GET)
	public String userRegistration(ModelMap model) { 
		
		RegistrationForm registrationForm = new RegistrationForm();
		
		model.addAttribute("registrationForm", registrationForm);
		
		return "registration";
	}

	@RequestMapping(value = { "/registration" }, method = RequestMethod.POST)
	public String userRegistrationSubmit(@Valid RegistrationForm form, BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			return "registration";
		}
		
		User user = new User();
		
		if (!this.userService.isUserLoginUnique(null, form.getLogin())) {
			
			FieldError loginError = this.getFieldError("user", "login", "non.unique.login", user.getLogin());
			
            result.addError(loginError);
			
            return "registration";
		}
		
		user
            .setLogin(form.getLogin())
            .setPassword(form.getPassword())
            .setEmail(form.getEmail())
            .setFirstName(form.getFirstName())
            .setLastName(form.getLastName());
		
		this.userService.save(user);
		
		model.addAttribute("success", "User " + user.getLogin() + " registered successfully");
		model.addAttribute("loggedinuser", this.getPrincipal());
		
		return "registrationsuccess";
	}
	
	@RequestMapping(value = "/access_denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("loggedinuser", this.getPrincipal());
        
		return "accessDenied";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage() {
		if (this.isCurrentAuthenticationAnonymous()) {
			return "login";
	    } else {
	    	return "redirect:/list";
	    }
	}

	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        
		if (auth != null){
			this.persistentTokenBasedRememberMeServices.logout(request, response, auth);
            
			SecurityContextHolder.getContext().setAuthentication(null);
		}
        
		return "redirect:/user/login?logout";
	}
}