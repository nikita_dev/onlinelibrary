package gfl.java.testtask.onlinelibrary.controller;

import gfl.java.testtask.onlinelibrary.file_utils.FileUtils;
import gfl.java.testtask.onlinelibrary.model.Book;
import gfl.java.testtask.onlinelibrary.model.User;
import gfl.java.testtask.onlinelibrary.model.UserRoleType;
import gfl.java.testtask.onlinelibrary.validator.ImageValidator;
import gfl.java.testtask.onlinelibrary.validator.TextfileValidator;
import java.io.FileNotFoundException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/admin")
@SessionAttributes("roles")
public class AdminController extends BaseController {
    private final static String VIEW_PREFIX = "/admin/";
    
    @Autowired
    private ImageValidator imageValidator;
    
    @Autowired
    private TextfileValidator textfileValidator;
	
    @RequestMapping(value = { "/list_user" }, method = RequestMethod.GET)
    public String listUser(ModelMap model) {
        List<User> users = this.userService.findAll("login");
        
        model.addAttribute("users", users);
        model.addAttribute("loggedinuser", this.getPrincipal());
        
        return VIEW_PREFIX + "userList";
    }
    
    @RequestMapping(value = { "/edit_user/{userId}" }, method = RequestMethod.GET)
    public String editUser(@PathVariable Integer userId, ModelMap model) {
        User user = this.userService.findById(userId);
        
        UserRoleType[] roleTypes = UserRoleType.values();
        
        model.addAttribute("user", user);
        model.addAttribute("roles", roleTypes);
        model.addAttribute("loggedinuser", this.getPrincipal());
        model.addAttribute("edit", true);
        
        return "registration";
    }
    
    @RequestMapping(value = { "/edit_user/{userId}" }, method = RequestMethod.POST)
    public String editUserSubmit(@Valid User user, BindingResult result, ModelMap model) {
        
        if (result.hasErrors()) {
			return "registration";
		}
        
        if (!this.userService.isUserLoginUnique(user.getId(), user.getLogin())) {
            FieldError loginError = this.getFieldError("user", "login", "non.unique.login", user.getLogin());
			
            result.addError(loginError);
			
            return "redirect:" + VIEW_PREFIX + "edit_user/" + user.getId();
		}
        
        this.userService.update(user);
        
        model.addAttribute("success", true);
        
        return "redirect:/" + VIEW_PREFIX + "userList?success";
    }
    
    @RequestMapping(value = { "/delete_user/{userId}" }, method = RequestMethod.GET)
    public String deleteUser(@PathVariable Integer userId, ModelMap model) {
        
        this.userService.delete(
            this.userService.findById(userId)
        );
        
        return "redirect:/" + VIEW_PREFIX + "userList?success";
    }
    
    @RequestMapping(value = {"/add_book"}, method = RequestMethod.GET)
    public String addBook(ModelMap model) {
		
        model.addAttribute("book", new Book());
        model.addAttribute("edit", false);
        model.addAttribute("loggedinuser", this.getPrincipal());
        
        return VIEW_PREFIX + "addBook";
    }
    
    @RequestMapping(value = { "/add_book", "/edit_book/{bookId}" }, method = RequestMethod.POST)
    public String addBookSubmit(@Valid Book book, @RequestParam("thumbnail") MultipartFile thumbnail, @RequestParam("bookFile") MultipartFile bookFile, HttpServletRequest request, BindingResult result, ModelMap model) {
        
        model.addAttribute("loggedinuser", this.getPrincipal());
		
		this.imageValidator.validate(thumbnail, result);
        
        this.textfileValidator.validate(bookFile, result);
		
        if (result.hasErrors()) {
            return VIEW_PREFIX + "addBook";
        }
		
		boolean isEdit = null != book.getId();
		
		Book currentBook;
		
		if (isEdit) {
			currentBook = this.bookService.findById(book.getId());
		}
		else {
			currentBook = book;
		}
        
        try {
            FileUtils thumbnailUtil = new FileUtils(thumbnail, request, currentBook.getThumbnailPath());

            if (!thumbnailUtil.couldSaveFile()) {
                FieldError imageError = this.getFieldError("image", "save", "couldnt.save.image", thumbnail.getName());

                result.addError(imageError);

                return VIEW_PREFIX + "addBook";
            }
            
            book.setThumbnailPath(thumbnailUtil.getResultPath());
        } catch (FileNotFoundException ex) {
            book.setThumbnailPath(currentBook.getThumbnailPath());
        }
        
        try {
            FileUtils textFileUtil = new FileUtils(bookFile, request, currentBook.getFilePath());

            if (!textFileUtil.couldSaveFile()) {
                FieldError textFileError = this.getFieldError("text", "save", "couldnt.save.textFile", bookFile.getName());

                result.addError(textFileError);

                return VIEW_PREFIX + "addBook";
            }
            
            book.setFilePath(textFileUtil.getResultPath());
        } catch (FileNotFoundException ex) {
			if (isEdit) {
				book.setFilePath(currentBook.getFilePath());
			}
			else {
				result.rejectValue("filePath", "missing.textFile");
				
				return VIEW_PREFIX + "addBook";
			}
        }
        
		if (isEdit) {
			this.bookService.update(book);
		}
		else {
			this.bookService.save(book);
		}
        
        return "redirect:/?success";
    }
    
    @RequestMapping(value = {"/edit_book/{bookId}"}, method = RequestMethod.GET)
    public String editBook(@PathVariable Integer bookId, ModelMap model) {
        Book book = this.bookService.findById(bookId);
        
        model.addAttribute("book", book);
		model.addAttribute("edit", true);
        model.addAttribute("loggedinuser", this.getPrincipal());
        
        return VIEW_PREFIX + "addBook";
    }
	
    @RequestMapping(value = {"/delete_book/{bookId}"}, method = RequestMethod.GET)
    public String deleteBook(@PathVariable Integer bookId, ModelMap model) {
        this.bookService.delete(
            this.bookService.findById(bookId)
        );
        
        return "redirect:/?success";
    }
}