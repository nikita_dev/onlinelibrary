package gfl.java.testtask.onlinelibrary.controller;

import gfl.java.testtask.onlinelibrary.model.Book;
import gfl.java.testtask.onlinelibrary.model.Comment;
import gfl.java.testtask.onlinelibrary.model.User;
import gfl.java.testtask.onlinelibrary.model.Vote;
import gfl.java.testtask.onlinelibrary.service.CommentService;
import gfl.java.testtask.onlinelibrary.service.VoteService;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class MainController extends BaseController {
	
	private static final String[] ALLOWED_SORTING_BY = {"title", "publishDate"};
    
	@Autowired
	private CommentService commentService;
    
    @Autowired
    private VoteService voteService;
	
	private String determOrderParam(Optional<String> orderBy) {
		return orderBy.isPresent() && 
				Arrays.asList(ALLOWED_SORTING_BY).contains(orderBy.get()) ? 
			orderBy.get() : 
			ALLOWED_SORTING_BY[0];
	}
	
	@RequestMapping(
		value = { 
			"/", 
			"/page/{pageNum}", 
			"/sort/{sortBy}",
			"/page/{pageNum}/sort/{sortBy}"
		}, method = RequestMethod.GET)
	public String listBooks(
		@PathVariable(value = "pageNum") Optional<Integer> pageNum, 
		@PathVariable(value = "sortBy") Optional<String> sortBy, 
		ModelMap model
	) {
		Integer currentPage = pageNum.isPresent() ? pageNum.get() : 1;
		
		String orderBy = this.determOrderParam(sortBy);
		
        List<Book> books = this.bookService.findAll(orderBy, currentPage);
        
        model.addAttribute("books", books);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("sortingBy", orderBy);
		model.addAttribute("pageCount", this.bookService.getPageCount());
        model.addAttribute("loggedinuser", this.getPrincipal());
        
		return "bookList";
	}
	
	@RequestMapping(value = { "/book/{bookId}" }, method = RequestMethod.GET)
	public String detailsBook(@PathVariable Integer bookId, ModelMap model) {
		Book book = this.bookService.findById(bookId);
        
        User user = this.userService.findByLogin(this.getPrincipal());
        
        Vote vote = this.voteService.findByBookAndUser(book, user);
        
        model.addAttribute("book", book);
        model.addAttribute("vote", vote);
        model.addAttribute("loggedinuser", this.getPrincipal());
        model.addAttribute("currentUser", this.userService.findByLogin(this.getPrincipal()));
		
		return "bookDetails";
	}
    
    @RequestMapping(
		value = { "/add_vote" }, 
		method = RequestMethod.POST, 
		produces = "application/json"
	)
    public @ResponseBody String userVote(
        @RequestParam("book_id") Integer bookId,
        @RequestParam("vote") boolean vote,
        ModelMap model
    ) {
        User user = this.userService.findByLogin(this.getPrincipal());
        
        Book book = this.bookService.findById(bookId);
        
        String response = "";
        
        if (!book.hasUserVoted(user)) {
            Vote voteIns = new Vote();
            
            voteIns
                .setBook(book)
                .setUser(user)
                .setVote(vote);
            
            this.voteService.save(voteIns);
            
            response = Long.toString(voteIns.getDate().getTime());
        }
        
        return response;
    }
	
	@RequestMapping(
		value = { "/add_comment" }, 
		method = RequestMethod.POST, 
		produces = "application/json"
	)
	public @ResponseBody Map<String, String> addComment(
		@RequestParam("book_id") Integer bookId, 
		@RequestParam("parent_id") Optional<Integer> parentId, 
		@RequestParam("text") String text, 
		ModelMap model
	) {
		User user = this.userService.findByLogin(this.getPrincipal());
		
		Book book = this.bookService.findById(bookId);
		
		Comment parentComment = null;
		
		if (parentId.isPresent()) {
			parentComment = this.commentService.findById(parentId.get());
		}
		
		Comment comment = new Comment();
		
		comment
			.setParentComment(parentComment)
			.setBook(book)
			.setText(text)
			.setUser(user);
		
		this.commentService.save(comment);
        
        Map<String, String> response = new HashMap<>();
        
        response.put("user", comment.getUser().getLogin());
        response.put("id", comment.getId().toString());
        response.put("date", Long.toString(comment.getDate().getTime()));
		
		return response;
	}
	
	@RequestMapping(
		value = { 
			"/book/search",
			"/book/sort/{sort}/search"
		}, 
		method = RequestMethod.GET
	)
	public String findBooks(
		@PathVariable(value = "sortBy") Optional<String> sortBy, 
		@RequestParam("target") String searchTarget, 
		ModelMap model
	) {
		
		String orderBy = this.determOrderParam(sortBy);
		
		model.addAttribute("target", searchTarget);
		model.addAttribute("isSearch", true);
		model.addAttribute("loggedinuser", this.getPrincipal());
		model.addAttribute("books", this.bookService.findByNameOrDescr(searchTarget, orderBy));
		
		return "bookList";
	}
	
	@RequestMapping(value = { "/book/download/{bookId}" }, method = RequestMethod.GET)
	public void downloadBook(HttpServletResponse response, @PathVariable("bookId") Integer bookId) throws IOException {
		Book book = this.bookService.findById(bookId);
		
		File file = new File(book.getFilePath());
		
		String mimeType = Files.probeContentType(file.toPath());
		
		response.setContentType(mimeType);
		response.setHeader("Content-Disposition", String.format("attachment; filename=%s", book.toString()));
		response.setContentLengthLong(file.length());
		
		InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
		
		FileCopyUtils.copy(inputStream, response.getOutputStream());
	}
}