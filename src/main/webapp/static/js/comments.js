$(document).ready(function () {
    var commentTemplate = $(
        "<div id='commentTemplate' class='list-group-item' style='display: none'>"
            +"<h4 class='list-group-item-heading'></h4>"
            +"<span class='badge'></span>"
            +"<p class='list-group-item-text'></p>"
            +"<button class='btn btn_add_subcomment btn-primary'>"
                +"<i class='fa fa-plus' aria-hidden='true'></i> "
                +"Answer"
            +"</button>"
        +"</div>");

    var bookId = $('#bookId');
    var commentText = $('#commentText');
	var commentsCount = $('#commentsCount');
    var csrf = $('#csrf_roken').val();

    $('#bookId').parent('.panel-body').on('click', '#btn_add_comment, .btn_add_subcomment', function () {
        var clickedButton = $(this);

        var parentComment = clickedButton.hasClass('btn_add_subcomment') ? clickedButton.attr('id') : null;

        $.post(
            '/OnlineLibrary/add_comment', 
            {
                book_id: bookId.val(),
                parent_id: parentComment,
                text: commentText.val(),
                _csrf: csrf
            }, function(response) {
                var newComment = commentTemplate.clone()
                    .find('.list-group-item-heading').text('User ' + response.user).end()
                    .find('.badge').text(
                        moment(
                            parseInt(response.date)
                        ).format('DD-MM-YYYY HH:mm:ss')
                    ).end()
                    .find('.list-group-item-text').text(commentText.val()).end()
                    .find('.btn_add_subcomment').attr('id', response.id).end()
                    .show();

                if(null === parentComment) {
                    var borderElem = bookId.prev();
					
					if ('noComment' === borderElem.attr('id')) {
						borderElem.wrap('<div class="list-group"></div>').replaceWith(newComment);
					}
					else {
						borderElem.append(newComment);
					}
                }
                else {
                    clickedButton.siblings('.list-group-item').andSelf().last().after(newComment);
                }

                commentText.val('');
				commentsCount.text(parseInt(commentsCount.text()) + 1);

                console.log(response);
            });
    });
});