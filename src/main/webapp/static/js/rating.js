$(document).ready(function () {
	var bookId = $('#bookId').val();
	var csrf = $('#csrf_roken').val();
	var voteBtns = $('.vote-btn');
	var voteVal = $('#voteVal');
	var votesCount = $('#userVotesCount');

	voteBtns.click(function () {
		var vote = $(this).attr('name') === '1' ? true : false;

		$.post(
			'/OnlineLibrary/add_vote',
			{
				book_id: bookId,
				vote: vote,
				_csrf: csrf
			}, function(response) {
				if(response !== '') {
					voteVal.text(parseInt(voteVal.text()) + (vote ? 1 : -1));

					voteBtns.hide();
					votesCount.text(parseInt(votesCount.text()) + 1);
				}

				console.log(response);
			});
	});
});