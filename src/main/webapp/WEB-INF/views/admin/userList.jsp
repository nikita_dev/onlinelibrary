<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
        <title>User list</title>
        <%@include file="/WEB-INF/views/partials/vendorStyles.jsp" %>
    </head>
    <body>
        <div class="container">
            <%@include file="/WEB-INF/views/partials/authheader.jsp" %>
            
            <c:if test="${null != param.success}">
                <div class="alert alert-success" role="alert">
                    Information is updated successfully.
                </div>
            </c:if>
            
            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th>Login</th>
                    <th>Email</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Role</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                <c:forEach items="${users}" var="user">
                    <tr>
                        <td>${user.login}</td>
                        <td>${user.email}</td>
                        <td>${user.firstName}</td>
                        <td>${user.lastName}</td>
                        <td>${user.userRole}</td>
                        <td>
                            <a class="btn btn-primary btn-block" role="button" href="<c:url value="/admin/edit_user/${user.id}" />">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                Edit
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-primary btn-block" role="button" href="<c:url value="/admin/delete_user/${user.id}" />">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                Delete
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>
