<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
        <title>JSP Page</title>
        <%@include file="/WEB-INF/views/partials/vendorStyles.jsp" %>
        
        <script src="<c:url value='/static/vendor/jquery/dist/jquery.js' />"></script>
        <script src="<c:url value='/static/vendor/bootstrap-file-input/bootstrap.file-input.js' />"></script>
        
        <script>
            $(document).ready(function () {
                $('input[type=file]').bootstrapFileInput();
            });
        </script>
    </head>
    <body>
        <div class="container">
            <%@include file="/WEB-INF/views/partials/authheader.jsp" %>
			
			<c:if test="${edit}">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 thumbnail">
					<img src="${book.getThumbnailBase64()}" alt="${book.title}"/>
				</div>
				<div class="row"></div>
			</c:if>
        
            <form:form method="POST" modelAttribute="book" class="form-horizontal" enctype="multipart/form-data">
                <form:input type="hidden" path="id" id="id"/>
                
                <div class="form-group">
                    <label class="col-md-3 control-lable" for="title">Title</label>
                    <div class="col-md-7">
                        <form:input type="text" path="title" id="title" class="form-control input-sm" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-lable" for="description">Description</label>
                    <div class="col-md-7">
                        <form:textarea path="description" id="description" class="form-control input-sm" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-lable" for="publisher">Publisher</label>
                    <div class="col-md-7">
                        <form:input type="text" path="publisher" id="publisher" class="form-control input-sm"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-lable" for="authors">Authors</label>
                    <div class="col-md-7">
                        <form:input type="text" path="authors" id="authors" class="form-control input-sm"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-lable" for="publishDate">Publish date</label>
                    <div class="col-md-7">
                        <form:input type="text" path="publishDate" id="publishDate" class="form-control input-sm" />
                    </div>
					<span id="publishDate" class="help-block">Please enter a date in format 'dd-mm-yyyy'</span>
                </div>
				
                <div class="form-group">
                    <label class="col-md-3 control-lable" for="bookThumbnail">Thumbnail</label>
                    <div class="col-md-7">
                        <input class="btn btn-primary" name="thumbnail" type="file" title="<i class='fa fa-file-image-o' aria-hidden='true'></i> Attach thumbnail" id="bookThumbnail"/>
                    </div>
					<span id="bookThumbnail" class="help-block">Allowed extensions are JPG, PNG, GIF</span>
                    
					<c:if test="${edit}">
						<span id="bookThumbnail" class="help-block">If you don't input an image, the old one will keep.</span>
					</c:if>
                </div>
                    
                <div class="form-group">
                    <label class="col-md-3 control-lable" for="bookFile">Source file</label>
                    <div class="col-md-7">
                        <input class="btn btn-primary" name="bookFile" type="file" title="<i class='fa fa-file-text-o' aria-hidden='true'></i> Attach book file" id="bookFile"/>
                    </div>
					<span id="bookFile" class="help-block">Allowed extensions are DOC, ODT, PDF, RTF, TXT</span>
                    
					<c:if test="${edit}">
						<span id="bookFile" class="help-block">If you don't input an file, the old one will keep.</span>
					</c:if>
                </div>
				
                <button class="btn btn-primary" type="submit">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    Submit
                </button>

                <spring:hasBindErrors name="book">
                    <c:if test="${errors != null}">
                        <div class="alert alert-danger" role="alert">
                            <form:errors path="*" class="help-inline"/>
                        </div>
                    </c:if>
                </spring:hasBindErrors>
            </form:form>
        </div>
    </body>
</html>
