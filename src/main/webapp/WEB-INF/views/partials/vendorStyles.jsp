<script src="<c:url value='/static/vendor/jquery/dist/jquery.js' />"></script>
<script src="<c:url value='/static/vendor/bootstrap/dist/js/bootstrap.js' />"></script>
<script src="<c:url value='/static/vendor/moment/moment.js' />"></script>

<link href="<c:url value='/static/vendor/bootstrap/dist/css/bootstrap.css' />" rel="stylesheet"/>
<link href="<c:url value='/static/vendor/font-awesome/css/font-awesome.css' />" rel="stylesheet"/>
<link href="<c:url value='/static/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css' />" rel="stylesheet"/>

<link href="<c:url value='/static/css/style.css' />" rel="stylesheet"/>