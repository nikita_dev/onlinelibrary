<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="list-group">
    <c:forEach var="comment" items="${comments}">
        <div class="list-group-item">
            <h4 class="list-group-item-heading">User ${comment.user.login}</h4>
            <span class="badge">
                <fmt:formatDate value="${comment.date}" pattern="dd-MM-yyyy HH:mm:ss" />
            </span>
            <p class="list-group-item-text">${comment.text}</p>

            <button id="${comment.id}" class="btn btn_add_subcomment btn-primary">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Answer
            </button>
        </div>
    </c:forEach>
</div>