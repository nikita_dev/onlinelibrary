<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="paginatingSize" value="1"/>

<nav aria-label="Page navigation">
	<ul class="pagination">
        
		<c:choose>
			<c:when test="${1 == currentPage}">
				<li class="disabled">
					<span>
						<span aria-hidden="true">&laquo;</span>
					</span>
				</li>
                <li class="active">
                    <span>1<span class="sr-only">(current)</span></span>
                </li>
			</c:when>
			<c:otherwise>
				<li>
					<a href="<c:url value="/page/${currentPage - 1}/sort/${sortingBy}" />" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
                <li>
                    <a href="<c:url value="/page/1/sort/${sortingBy}" />">1</a>
                </li>
			</c:otherwise>
		</c:choose>
                
        <c:choose>
            <c:when test="${2 < currentPage}">
                <c:set var="paginatingStart" value="${currentPage - paginatingSize}"/>
            </c:when>
            <c:otherwise>
                <c:set var="paginatingStart" value="2"/>
            </c:otherwise>
        </c:choose>
                
        <c:choose>
            <c:when test="${currentPage + paginatingSize < pageCount}">
                <c:set var="paginatingFinish" value="${currentPage + paginatingSize}"/>
            </c:when>
            <c:otherwise>
                <c:set var="paginatingFinish" value="${pageCount - 1}"/>
            </c:otherwise>
        </c:choose>
                
		<c:forEach var="i" begin="${paginatingStart}" end="${paginatingFinish}">
            <c:choose>
				<c:when test="${i == currentPage}">
					<li class="active">
						<span>${i}<span class="sr-only">(current)</span></span>
					</li>
				</c:when>
				<c:otherwise>
					<li><a href="<c:url value="/page/${i}/sort/${sortingBy}" />">${i}</a></li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
                    
		<c:choose>
			<c:when test="${pageCount == currentPage}">
                <li class="active">
                    <span>${pageCount}<span class="sr-only">(current)</span></span>
                </li>
				<li class="disabled">
					<span>
						<span aria-hidden="true">&raquo;</span>
					</span>
				</li>
			</c:when>
			<c:otherwise>
                <li>
                    <a href="<c:url value="/page/${pageCount}/sort/${sortingBy}" />">${pageCount}</a>
                </li>
				<li>
					<a href="<c:url value="/page/${currentPage + 1}/sort/${sortingBy}" />" aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
			</c:otherwise>
		</c:choose>
                
	</ul>
</nav>