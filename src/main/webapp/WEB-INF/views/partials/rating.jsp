<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<p>Rating</p>
<div class="btn-group-vertical" role="group" aria-label="...">
	<c:if test="${!book.hasUserVoted(currentUser)}">
		<button name="1" class="btn btn-primary vote-btn">
			<i class="fa fa-thumbs-o-up fa-fw" aria-hidden="true"></i>
		</button>
	</c:if>
	<span id="voteVal" style="width: 100%" class="badge">
		${book.getRating()}
	</span>
	<c:if test="${!book.hasUserVoted(currentUser)}">
		<button name="0" class="btn btn-primary vote-btn">
			<i class="fa fa-thumbs-o-down fa-fw" aria-hidden="true"></i>
		</button>
	</c:if>
</div>
<c:if test="${!empty(vote)}">
	<div class="well">
		You have already voted as
		<c:choose>
			<c:when test="${vote.vote == 1}">
				<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
			</c:when>
			<c:otherwise>
				<i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
			</c:otherwise>
		</c:choose>
		for this book on 
		<fmt:formatDate value="${vote.date}" pattern="dd-MM-yyyy HH:mm:ss" />
	</div>
</c:if>