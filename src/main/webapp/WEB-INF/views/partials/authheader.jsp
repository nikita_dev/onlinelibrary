<div>
    <span>Dear <strong>${loggedinuser}</strong>, Welcome to Online library.</span>
    
    <a class="btn btn-primary" role="button" href="<c:url value="/" />">
        <i class="fa fa-home" aria-hidden="true"></i>
        Main
    </a>
    
    <a class="btn btn-primary" role="button" href="<c:url value="/user/logout" />">
        <i class="fa fa-sign-out" aria-hidden="true"></i>
        Logout
    </a>
</div>