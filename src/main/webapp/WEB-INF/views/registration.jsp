<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>User Registration Form</title>
    <%@include file="/WEB-INF/views/partials/vendorStyles.jsp" %>
</head>

<body>
 	<div class="container">
        
        <c:choose>
            <c:when test="${edit}">
                <%@include file="/WEB-INF/views/partials/authheader.jsp" %>
                
                <a class="btn btn-primary" role="button" href="<c:url value="/admin/list_user" />">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    User list
                </a>
            </c:when>
            <c:otherwise>
                <a class="btn btn-primary" role="button" href="<c:url value="/user/login" />">
                    <i class="fa fa-sign-in" aria-hidden="true"></i>
                    Login
                </a>
                    
                <div class="well lead">User Registration Form</div>
            </c:otherwise>
        </c:choose>
        
        <form:form method="POST" modelAttribute="${edit ? 'user' : 'registrationForm'}" class="form-horizontal">
            
            <form:input type="hidden" path="id" id="id"/>
            
            <div class="form-group">
                <label class="col-md-3 control-lable" for="login">Login</label>
                <div class="col-md-7">
                    <form:input type="text" path="login" id="login" class="form-control input-sm" />
                </div>
            </div>
	
            <c:if test="${!edit}">
                <div class="form-group">
                    <label class="col-md-3 control-lable" for="password">Password</label>
                    <div class="col-md-7">
                        <form:input type="password" path="password" id="password" class="form-control input-sm" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-lable" for="confirmationPassword">Confirm password</label>
                    <div class="col-md-7">
                        <form:input type="password" path="confirmationPassword" id="confirmationPassword" class="form-control input-sm" />
                    </div>
                </div>
            </c:if>
	
            <div class="form-group">
                <label class="col-md-3 control-lable" for="email">Email</label>
                <div class="col-md-7">
                    <form:input type="text" path="email" id="email" class="form-control input-sm" />
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-3 control-lable" for="firstName">First Name</label>
                <div class="col-md-7">
                    <form:input type="text" path="firstName" id="firstName" class="form-control input-sm"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-lable" for="lastName">Last Name</label>
                <div class="col-md-7">
                    <form:input type="text" path="lastName" id="lastName" class="form-control input-sm" />
                </div>
            </div>

            <c:choose>
                <c:when test="${edit}">
                    <div class="form-group">
                        <label class="col-md-3 control-lable" for="userRole">Role</label>
                        <div class="col-md-7">
                            <form:select path="userRole" items="${roles}" id="userRole" class="form-control" >
                            </form:select>
                        </div>
                    </div>
                
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        Submit
                    </button>
                </c:when>
                <c:otherwise>
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                        Registration
                    </button>
                </c:otherwise>
            </c:choose>
					
			<spring:hasBindErrors name="registrationForm">
				<c:if test="${errors != null}">
					<div class="alert alert-danger" role="alert">
						<form:errors path="*" class="help-inline"/>
					</div>
				</c:if>
			</spring:hasBindErrors>
			
		</form:form>
	</div>
</body>
</html>