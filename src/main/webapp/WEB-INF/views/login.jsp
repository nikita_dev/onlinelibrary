<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Login page</title>
		<%@include file="/WEB-INF/views/partials/vendorStyles.jsp" %>
	</head>
	<body>
        <div class="container">

            <a class="btn btn-primary" role="button" href="<c:url value="/user/registration" />">
                <i class="fa fa-user-plus" aria-hidden="true"></i>
                Registration
            </a>
                
            <div class="well lead">User Login Form</div>

            <form action="<c:url value="/user/login" />" method="post" class="form-horizontal">

                <c:if test="${null != param.error}">
                    <div class="alert alert-danger">
                        <p>Invalid username and password.</p>
                    </div>
                </c:if>

                <c:if test="${null != param.logout}">
                    <div class="alert alert-success">
                        <p>You have been logged out successfully.</p>
                    </div>
                </c:if>

                <div class="input-group">
                    <label class="input-group-addon" for="username"><i class="fa fa-user fa-fw"></i></label>
                    <input type="text" class="form-control" id="username" name="login" placeholder="Enter Username">
                </div>

                <div class="input-group">
                    <label class="input-group-addon" for="password"><i class="fa fa-lock fa-fw"></i></label> 
                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password">
                </div>

                <div class="checkbox checkbox-primary">
                  <input class="styled" type="checkbox" id="rememberme" name="remember-me">
                  <label for="rememberme">Remember Me</label>
                </div>

                <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />

                <button class="btn btn-primary">
                    <i class="fa fa-sign-in" aria-hidden="true"></i>
                    Login
                </button>
            </form>
        </div>
	</body>
</html>