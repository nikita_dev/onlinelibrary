<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Book list</title>
        <%@include file="/WEB-INF/views/partials/vendorStyles.jsp" %>
    </head>
    <body>
        <div class="container">
            <%@include file="/WEB-INF/views/partials/authheader.jsp" %>
            
            <c:if test="${null != param.success}">
                <div class="alert alert-success" role="alert">
                    Information is updated successfully.
                </div>
            </c:if>
			
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Sorting by <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<c:choose>
								<c:when test="${isSearch}">
									<li><a href="<c:url value="/book/sort/title/search?target=${target}" />">Title</a></li>
									<li><a href="<c:url value="/book/sort/publishDate/search?target=${target}" />">Publish date</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="<c:url value="/page/${currentPage}/sort/title" />">Title</a></li>
									<li><a href="<c:url value="/page/${currentPage}/sort/publishDate" />">Publish date</a></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
				</div>

				<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-xs-12">
					<form method="get" action="<c:url value="/book/search" />" class="form-horizontal">
						<div class="input-group">
							<input type="text" name="target" class="form-control" placeholder="Search for..." value="${target}" />
							<span class="input-group-btn">
								<button class="btn btn-primary" type="submit">
									<i class="fa fa-search" aria-hidden="true"></i>
									Search
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			
			<c:if test="${!isSearch}">
				<%@include file="/WEB-INF/views/partials/paginator.jsp" %>
			</c:if>
            
            <div class="row">
				<c:if test="${isSearch && 0 == fn.length(books)}">
					<div class="well">
						No result
					</div>
				</c:if>
                <c:forEach items="${books}" var="book">
                    <div class="col-lg-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                ${book.title}
                                <span class="badge pull-right">
									Rating
									${book.getRating()}
								</span>
								<span class="badge pull-right">
									Comments
									${fn:length(book.comments)}
								</span>
                            </div>
                            <div class="panel-body">
								
                                <c:if test="${null != book.thumbnailPath}">
                                    <div class="thumbnail">
                                        <img src="${book.getThumbnailBase64()}" alt="${book.title}"/>
                                    </div>
                                </c:if>
                                
                                <div class="row">
                                    <div class="col-lg-6 col-md-8 col-sm-8 col-xs-6">
                                        <dl>
                                            <dt>Authors</dt>
                                            <dd>${book.authors}</dd>
                                            
                                            <dt>Published</dt>
                                            <dd><fmt:formatDate value="${book.publishDate}" pattern="dd-MM-yyyy" /></dd>
                                            
                                            <dt>Publisher</dt>
                                            <dd>${book.publisher}</dd>
                                        </dl>
                                    </div>
                                    <div class="col-lg-6 col-md-4 col-sm-4 col-xs-6 pull-right">
                                        <a class="btn btn-primary btn-block" role="button" href="<c:url value="/book/${book.id}" />">
                                            <i class="fa fa-info" aria-hidden="true"></i>
                                            See details
                                        </a>
                                        <sec:authorize access="hasRole('ADMIN')">
                                            <a class="btn btn-primary btn-block" role="button" href="<c:url value="/admin/edit_book/${book.id}" />">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                                Edit book
                                            </a>
                                            <a class="btn btn-primary btn-block" role="button" href="<c:url value="/admin/delete_book/${book.id}" />">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                Delete book
                                            </a>
                                        </sec:authorize>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
            
            <sec:authorize access="hasRole('ADMIN')">
                <a class="btn btn-primary" role="button" href="<c:url value="/admin/add_book" />">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    Add book
                </a>
                    
                <a class="btn btn-primary" role="button" href="<c:url value="/admin/list_user" />">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    User list
                </a>
            </sec:authorize>
        </div>
    </body>
</html>
