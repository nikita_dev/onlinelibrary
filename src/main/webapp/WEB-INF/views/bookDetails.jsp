<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="comment" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
        <title>${book.title}</title>
		<%@include file="/WEB-INF/views/partials/vendorStyles.jsp" %>
        <script src="<c:url value='/static/js/comments.js' />"></script>
        <c:if test="${!book.hasUserVoted(currentUser)}">
            <script src="<c:url value='/static/js/rating.js' />"></script>
        </c:if>
    </head>
    <body>
		<div class="container">
			<%@include file="/WEB-INF/views/partials/authheader.jsp" %>
			
			<div class="panel panel-primary">
				<div class="panel-heading">
					${book.title}
				</div>
				<div class="panel-body">
                    
                    <c:if test="${null != book.thumbnailPath}">
                        <div class="thumbnail">
                            <img src="${book.getThumbnailBase64()}" alt="${book.title}"/>
                        </div>
                    </c:if>
                    <div class="row">
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <dl>
                                <dt>Authors</dt>
                                <dd>${book.authors}</dd>

                                <dt>Published</dt>
                                <dd><fmt:formatDate value="${book.publishDate}" pattern="dd-MM-yyyy" /></dd>

                                <dt>Publisher</dt>
                                <dd>${book.publisher}</dd>

                                <dt>Description</dt>
                                <dd>${book.description}</dd>
                            </dl>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <a class="btn btn-primary" role="button" href="<c:url value="/book/download/${book.id}" />">
                                <i class="fa fa-download" aria-hidden="true"></i>
                                Download
                            </a>
							<p id="votedUsers">
								Total voted users 
								<span id="userVotesCount" class="badge">
                                    ${fn:length(book.votes)}
                                </span>
							</p>
							<%@include file="/WEB-INF/views/partials/rating.jsp" %>
                        </div>
                    </div>
					
					<div class="panel panel-primary">
						<div class="panel-heading">
							Comments
							<span id="commentsCount" class="badge">
								${fn:length(book.comments)}
							</span>
						</div>
						<div class="panel-body">
							<c:choose>
								<c:when test="${fn:length(book.comments) > 0}">
									<div class="list-group">
										<c:forEach var="comment" items="${book.comments}">
                                            <c:if test="${null == comment.parentComment}">
                                                <div class="list-group-item">
                                                    <h4 class="list-group-item-heading">User ${comment.user.login}</h4>
                                                    <span class="badge">
                                                        <fmt:formatDate value="${comment.date}" pattern="dd-MM-yyyy HH:mm:ss" />
                                                    </span>
                                                    <p class="list-group-item-text">${comment.text}</p>

                                                    <button id="${comment.id}" class="btn btn_add_subcomment btn-primary">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                        Answer
                                                    </button>
                                                    
                                                    <c:if test="${fn:length(comment.subComments) > 0}">
                                                        <comment:commentGroup comments="${comment}"/>
                                                    </c:if>
                                                </div>
                                            </c:if>
										</c:forEach>
									</div>
								</c:when>
								<c:otherwise>
									<p id="noComment">There are no any comment, be first.</p>
								</c:otherwise>
							</c:choose>
									
							<input id="bookId" type="hidden" name="book_id" value="${book.id}"/>

							<textarea id="commentText" class="form-control" name="text" rows="3"></textarea>

							<input id="csrf_roken" type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

							<button id="btn_add_comment" class="btn btn-primary">
								<i class="fa fa-plus" aria-hidden="true"></i>
								Add comment
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
    </body>
</html>
