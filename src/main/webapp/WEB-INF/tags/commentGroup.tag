<%@ attribute name="comments" required="true" type="gfl.java.testtask.onlinelibrary.model.Comment"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="comment" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:forEach var="comment" items="${comments.subComments}">
    <div class="list-group-item">
        <h4 class="list-group-item-heading">User ${comment.user.login}</h4>
        <span class="badge">
            <fmt:formatDate value="${comment.date}" pattern="dd-MM-yyyy HH:mm:ss" />
        </span>
        <p class="list-group-item-text">${comment.text}</p>

        <button id="${comment.id}" class="btn btn_add_subcomment btn-primary">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Answer
        </button>
        <c:if test="${fn:length(comment.subComments) > 0}">
            <comment:commentGroup comments="${comment}"/>
        </c:if>
    </div>
</c:forEach>