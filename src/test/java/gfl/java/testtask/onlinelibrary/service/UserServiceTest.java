package gfl.java.testtask.onlinelibrary.service;

import gfl.java.testtask.onlinelibrary.dao.UserDaoInterface;
import gfl.java.testtask.onlinelibrary.model.User;
import gfl.java.testtask.onlinelibrary.service.implementations.UserServiceImpl;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InjectMocks;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

public class UserServiceTest {
	
	@Mock
	private UserDaoInterface userDao;
	
	@InjectMocks
	private UserServiceImpl userService;
	
	@Spy
	private List<User> users = new ArrayList<>();
	
	public List<User> getUserList() {
		User u1 = new User();
		u1
			.setId(1)
			.setLogin("test1")
			.setEmail("asd@i.ua")
			.setPassword("123123")
			.setUserRole("ROLE_ADMIN");
		
		User u2 = new User();
		u2
			.setId(2)
			.setLogin("test2")
			.setEmail("qwe@i.ua")
			.setPassword("123123")
			.setUserRole("ROLE_USER");
		
		this.users.add(u1);
		this.users.add(u2);
		
		return this.users;
	}
	
	@BeforeClass
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		this.users = this.getUserList();
	}
	
	@Test
	public void findById() {
		User user = this.users.get(0);
		
		when(this.userDao.findById(anyInt())).thenReturn(user);
		
		Assert.assertEquals(this.userService.findById(user.getId()), user);
	}
	
	@Test
	public void findByLogin() {
		User user = this.users.get(0);
		
		when(this.userDao.findByLogin(anyString())).thenReturn(user);
		
		Assert.assertEquals(this.userService.findByLogin(user.getLogin()), user);
	}
	
	@Test
	public void isUserLoginUnique() {
		User user = this.users.get(0);
		
		when(this.userDao.findByLogin(anyString())).thenReturn(user);
		
		Assert.assertEquals(this.userService.isUserLoginUnique(user.getId(), user.getLogin()), true);
	}
	
	@Test
	public void delete() {
		doNothing().when(this.userDao).delete(any(User.class));
		
		this.userService.delete(any(User.class));
		
		verify(this.userDao, only()).delete(any(User.class));
	}
}