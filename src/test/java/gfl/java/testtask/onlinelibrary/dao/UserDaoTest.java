package gfl.java.testtask.onlinelibrary.dao;

import gfl.java.testtask.onlinelibrary.model.User;
import java.util.List;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UserDaoTest extends DaoImplTest {
	
	@Autowired
	private UserDaoInterface userDao;
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Users.xml"));
	}
	
	public User getFixture() {
		User user = new User();
		user
			.setLogin("asdqwe")
			.setPassword("123123")
			.setUserRole("ROLE_ADMIN")
			.setEmail("test@test.ua")
			.setFirstName("testFirstName")
			.setLastName("testLastName");
		
		return user;
	}
	
	@Test
	public void findByLogin() {
		Assert.assertNotNull(this.userDao.findByLogin("qweasd"));
		Assert.assertNull(this.userDao.findByLogin("test"));
	}
	
	@Test
	public void findById() {
		Assert.assertNotNull(this.userDao.findById(2));
		Assert.assertNull(this.userDao.findById(1));
	}
	
	@Test
	public void findAll() {
		List<User> result = this.userDao.findAll("id");
		Assert.assertNotNull(result);
		Assert.assertEquals(result.size(), 2);
	}
	
	@Test
	public void save() {
		this.userDao.save(
			this.getFixture()
		);
		
		List<User> result = this.userDao.findAll("id");
		
		Assert.assertEquals(result.size(), 3);
		Assert.assertEquals(result.get(0).getLogin(), this.getFixture().getLogin());
	}
	
	@Test
	public void update() {
		final String newEmail = "test1@test.ua";
		final int testingId = 2;
		
		User user = 
			this.userDao.findById(testingId)
			.setEmail(newEmail);
		
		this.userDao.update(user);
		
		Assert.assertEquals(newEmail, this.userDao.findById(testingId).getEmail());
	}
	
	@Test
	public void delete() {
		List<User> listBeforeDelete = this.userDao.findAll("id");
		
		this.userDao.delete(
			this.userDao.findById(3)
		);
		
		List<User> listAfterDelete = this.userDao.findAll("id");
		
		Assert.assertEquals(listBeforeDelete.size() - 1, listAfterDelete.size());
		Assert.assertNull(this.userDao.findById(3));
	}
}