package gfl.java.testtask.onlinelibrary.dao;

import gfl.java.testtask.onlinelibrary.configuration.HibernateTestConfiguration;
import javax.sql.DataSource;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;

@ContextConfiguration(classes = { HibernateTestConfiguration.class })
abstract public class DaoImplTest extends AbstractTransactionalTestNGSpringContextTests {
	@Autowired
    private DataSource dataSource;
	
	protected abstract IDataSet getDataSet() throws Exception;
 
    @BeforeMethod
    public void setUp() throws Exception {
        IDatabaseConnection dbConn = new DatabaseDataSourceConnection(dataSource);
        DatabaseOperation.CLEAN_INSERT.execute(dbConn, getDataSet());
    }
}